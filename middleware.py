import os
import sys
import json
import time

# external dependencies need to be loaded from bundled dir
package_dir = os.path.abspath(os.path.dirname(__file__))
bundle_dir = "vendor/python/site-packages/"
sys.path.append(os.path.join(package_dir, bundle_dir))

import gitlab
import requests
from tyk.decorators import Hook
from tyk.gateway import TykGateway as tyk

from check import Check, AUTH_FIELDS

UNFURL_CLOUD_URL = os.environ["UNFURL_CLOUD_URL"]
assert UNFURL_CLOUD_URL and len(UNFURL_CLOUD_URL) > 0
print(f"UF: will auth against {UNFURL_CLOUD_URL}")


def get_auth_cookies(request):
    cookies_header = request.get_header("Cookie") or ""

    # [ ['_gitlab_session*', 'value'], ... ]
    return [
        c.split("=", 1)
        for c in cookies_header.split(";")
        if c.strip().startswith("_gitlab_session")
    ]


# Tyk will only use one header or param to check cache, but we are using
# multiple query params or headers to specify a) what project to auth against,
# b) access token, and c) target privilege. So, combine all these together into
# something we can use to uniquely identify a request and cache using that.
@Hook
def UnfurlAuthBuildHeader(request, session, spec):
    tyk.log("UF: building cache header", "debug")

    # try to get auth fields
    access_token = request.object.params["private_token"] or request.get_header(
        "Private-Token"
    )
    cookies = ";".join(["=".join(c) for c in get_auth_cookies(request)])

    auth = access_token or cookies
    params = build_search_params(request, spec)

    request.add_header("X-Unfurl-Authorization", f"{auth}|{params}")

    return request, session


# remove our caching header so it does not leak anything to proxied service
@Hook
def UnfurlAuthRemoveHeader(request, session, spec):
    request.delete_header("X-Unfurl-Authorization")

    # set header here based on cached metadata
    request.add_header("X-Unfurl-Authtype", session.metadata["authtype"])

    return request, session


@Hook
def UnfurlAuthCheck(request, session, metadata, spec):
    # tyk.log("UF: ", "debug")
    tyk.log("UF: verifying request:", "debug")
    # tyk.log(f"UF:   headers: {request.object.headers}", "debug")
    # tyk.log(f"UF:   params: {request.object.params}", "debug")
    # tyk.log(f"UF:   body: {request.object.body}", "debug")
    # tyk.log(f"UF:   api config: {spec['config_data']}", "debug")

    # try to get auth fields
    cookies_header = request.get_header("Cookie")
    access_token = request.object.params["private_token"] or request.get_header(
        "Private-Token"
    )

    # assume guest access
    is_guest = True

    # try access token as param or header
    if access_token is not None and access_token != "":
        tyk.log("UF:   using access token", "debug")
        gl = gitlab.Gitlab(url=UNFURL_CLOUD_URL, private_token=access_token)
        try:
            gl.auth()
            is_guest = False

        except gitlab.exceptions.GitlabAuthenticationError:
            tyk.log("UF: auth failed", "debug")
            # immediately fail if token auth fails
            return request, session, metadata

    # or use session cookie if present
    elif cookies_header is not None and cookies_header != "":
        tyk.log("UF:   using cookie auth", "debug")
        gl_session = requests.Session()

        for name, value in get_auth_cookies(request):
            gl_session.cookies.set(name, value)

        gl = gitlab.Gitlab(url=UNFURL_CLOUD_URL, session=gl_session)
        try:
            gl.auth()
            is_guest = False

        # if session auth fails, use guest access
        # (guest users still have a session cookie but it won't work here)
        except gitlab.exceptions.GitlabAuthenticationError:
            tyk.log("UF: session auth failed, proceeding as guest", "debug")
            gl = gitlab.Gitlab(url=UNFURL_CLOUD_URL)

    # if no auth given, proceed as guest
    else:
        tyk.log("UF: no auth given, proceeding as guest", "debug")
        gl = gitlab.Gitlab(url=UNFURL_CLOUD_URL)


    # second client without auth to check for public repos
    anon_gl = gitlab.Gitlab(url=UNFURL_CLOUD_URL)


    # build search parameters from ai settings and request
    search_params = build_search_params(request, spec)
    check = Check(gl, search_params, is_guest)

    # fetch result
    (username, auth_type) = check.allowed()
    if username:
        tyk.log(f"UF: user {username} is valid for {search_params}", "info")

        # approve session
        session.is_inactive = False

        # set session to expire in 5 mins, or limit from api definition
        ttl = search_params.get("ttl", 500)
        tyk.log(f"UF:   caching for {ttl} seconds", "debug")
        expire_at = int(time.time()) + int(ttl)
        session.expires = expire_at
        session.id_extractor_deadline = expire_at

        # set limits from api spec
        session.rate = check.get_level("user_rate", 100)
        session.per = check.get_level("user_per", 10)
        tyk.log(f"UF:   limit: {session.rate}x per {session.per}s", "debug")

        # token used to cache responses
        # user|params is our permission boundary
        metadata["token"] = f"{username}|{search_params}"

        # set username for other checks later
        metadata["username"] = username

        # set auth-type metadata for header
        metadata["authtype"] = auth_type

    return request, session, metadata


# merge params from api definition and search params to build target
def build_search_params(request, spec):
    # extract fields from request params
    params = {f: request.object.params.get(f) for f in AUTH_FIELDS}

    # overwrite with values set in api config
    config_data = json.loads(spec["config_data"])
    params.update(config_data)

    # add HTTP request type to params
    params.update(method=request.object.method)

    return params


@Hook
def UnfurlResponseHeaders(request, response, session, metadata, spec):
    tyk.log(f"UF:   setting response headers: ", "debug")

    # set header here based on cached metadata
    response.headers["X-Unfurl-Authtype"] = session.metadata["authtype"]

    return response
