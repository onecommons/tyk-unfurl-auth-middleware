# frozen_string_literal: true

require 'inspec/resources/http'

class ApiService < Http
  name 'api_service'
  desc 'Wraps http resource with custom data'

  supports platform: 'unix'

  def initialize(service_path, opts = {})
    url = URI.join(input('tyk_url'), '/services/', service_path)
    super(url.to_s, opts)
  end

  def to_s
    'API Service'
  end
end

class ::Inspec::Resources::Http
  def to_s
    # hide url
    "API call"
  end
end
