# frozen_string_literal: true

title 'Read/Write api'

url = input('tyk_url')
users = input('users')
projects = input('projects')

control 'rw_guest_read' do
  title 'Anonymous access, public read'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'rw_guest_write' do
  title 'Anonymous access, developer write'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'rw_user_read' do
  title 'Non-member access, public read'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'rw_user_write' do
  title 'Non-member access, developer write'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'rw_member_read' do
  title 'Non-developer member access, public read'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:member] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:member] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:member] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end
end

control 'rw_member_write' do
  title 'Non-developer member access, developer write'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:member] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:member] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:member] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'rw_developer_read' do
  title 'Developer access, public read'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:developer] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:developer] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:developer] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end
end

control 'rw_developer_write' do
  title 'Developer access, developer write'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:developer] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:developer] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:developer] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end
end

control 'rw_admin_read' do
  title 'Admin access, public read'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end
end

control 'rw_admin_write' do
  title 'Admin access, developer write'

  describe 'public project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:public] },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'internal project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:internal] },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end

  describe 'private project' do
    describe http("#{url}/services/read-write/anything",
                  params: { auth_project: projects[:private] },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' },
                  method: 'POST') do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
    end
  end
end
