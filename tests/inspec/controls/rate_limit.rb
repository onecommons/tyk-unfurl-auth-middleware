# frozen_string_literal: true

title 'Rate Limits'

url = input('tyk_url')

control 'lowered_limits' do
  title '2 reqs every 60 seconds'

  2.times do
    describe http("#{url}/services/limited-test/anything") do
      its('status') { should_not eq 429 }
    end
  end

  describe http("#{url}/services/limited-test/anything") do
    its('status') { should eq 429 }
    its('body') { should match(/"error": "Rate limit exceeded"/) }
  end

  # 60s is too long to wait
end

control 'default_limits' do
  title '100 reqs every 10 seconds'

  # TODO: disable this test for now
  # TODO: inspec is not reliably fast enough to hit the default limit

  # # initial request
  # describe http("#{url}/services/none-test/anything") do
  #   its('status') { should_not eq 429 }
  # end

  # 100.times do
  #   # faster than using inspec http() each time
  #   Faraday.get("#{url}/services/none-test/anything")
  # end

  # # after rate limit hit
  # describe http("#{url}/services/none-test/anything") do
  #   its('status') { should eq 429 }
  #   its('body') { should match(/"error": "Rate limit exceeded"/) }
  # end

  # describe http("#{url}/services/none-test/anything") do
  #   # wait for rate limit to expire
  #   before { sleep 10 }

  #   its('status') { should_not eq 429 }
  # end
end

control 'rw_limits_read' do
  title '10 reqs every 2 seconds'

  10.times do
    describe http("#{url}/services/limited-rw-test/anything") do
      its('status') { should_not eq 429 }
    end
  end

  describe http("#{url}/services/limited-rw-test/anything") do
    its('status') { should eq 429 }
    its('body') { should match(/"error": "Rate limit exceeded"/) }
  end

  describe http("#{url}/services/limited-rw-test/anything") do
    # wait for rate limit to expire
    before { sleep 2 }

    its('status') { should_not eq 429 }
  end
end

control 'rw_limits_write' do
  title '2 reqs every 5 seconds'

  2.times do
    describe http("#{url}/services/limited-rw-test/anything",
                  method: 'POST') do
      its('status') { should_not eq 429 }
    end
  end

  describe http("#{url}/services/limited-rw-test/anything",
                method: 'POST') do
    its('status') { should eq 429 }
    its('body') { should match(/"error": "Rate limit exceeded"/) }
  end

  describe http("#{url}/services/limited-rw-test/anything",
                method: 'POST') do
    # wait for rate limit to expire
    before { sleep 5 }

    its('status') { should_not eq 429 }
  end
end
