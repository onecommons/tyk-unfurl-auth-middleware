# frozen_string_literal: true

title 'Project access'

url = input('tyk_url')
users = input('users')
projects = input('projects')

control 'project_guest' do
  title 'Anonymous access'

  describe 'public project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'public' },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'maintainer' },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end

  describe 'internal project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'public' },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'maintainer' },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end

  describe 'private project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'public' },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'maintainer' },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end
end

control 'project_user' do
  title 'Normal user, not a member'

  describe 'public project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:normal] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:normal] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end

  describe 'internal project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:normal] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:normal] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end

  describe 'private project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:normal] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:normal] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end
end

control 'project_member' do
  title 'User with developer membership'

  describe 'public project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
      end
    end

    describe 'developer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'developer' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end

  describe 'internal project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'developer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'developer' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end

  describe 'private project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'developer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'developer' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:developer] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 403 }
        its('body') { should match(/"error": "Forbidden"/) }
      end
    end
  end
end

control 'project_admin' do
  title 'Maintainer membership'

  describe 'public project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:admin] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:public], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:admin] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end
  end

  describe 'internal project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:admin] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:internal], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:admin] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end
  end

  describe 'private project' do
    describe 'public auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'public' },
                    headers: { PRIVATE_TOKEN: users[:admin] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end

    describe 'maintainer auth' do
      describe http("#{url}/services/none-test/anything",
                    params: { auth_project: projects[:private], auth_project_access: 'maintainer' },
                    headers: { PRIVATE_TOKEN: users[:admin] },
                    data: { testdata: 'foo' }) do
        its('status') { should eq 200 }
        its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
      end
    end
  end
end
