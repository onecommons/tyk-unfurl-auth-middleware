# frozen_string_literal: true

title 'User access'

url = input('tyk_url')
users = input('users')

control 'user_guest' do
  title 'Anonymous access'

  describe 'no auth checks' do
    describe http("#{url}/services/none-test/anything",
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
    end
  end

  describe 'must be external' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'external' },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'must be normal' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'normal' },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'must be admin' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'admin' },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'user_external' do
  title 'External user'

  describe 'no auth checks' do
    describe http("#{url}/services/none-test/anything",
                  headers: { PRIVATE_TOKEN: users[:external] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
    end
  end

  describe 'must be external' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'external' },
                  headers: { PRIVATE_TOKEN: users[:external] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
    end
  end

  describe 'must be normal' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'normal' },
                  headers: { PRIVATE_TOKEN: users[:external] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end

  describe 'must be admin' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'admin' },
                  headers: { PRIVATE_TOKEN: users[:external] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'user_normal' do
  title 'Normal, non-external user'

  describe 'no auth checks' do
    describe http("#{url}/services/none-test/anything",
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
    end
  end

  describe 'must be external' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'external' },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
    end
  end

  describe 'must be normal' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'normal' },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
    end
  end

  describe 'must be admin' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'admin' },
                  headers: { PRIVATE_TOKEN: users[:normal] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 403 }
      its('body') { should match(/"error": "Forbidden"/) }
    end
  end
end

control 'user_admin' do
  title 'Admininstrator user'

  describe 'no auth checks' do
    describe http("#{url}/services/none-test/anything",
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "public" }
    end
  end

  describe 'must be external' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'external' },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
    end
  end

  describe 'must be normal' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'normal' },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
    end
  end

  describe 'must be admin' do
    describe http("#{url}/services/none-test/anything",
                  params: { auth_access: 'admin' },
                  headers: { PRIVATE_TOKEN: users[:admin] },
                  data: { testdata: 'foo' }) do
      its('status') { should eq 200 }
      its('body') { should match(/"testdata": "foo"/) }
        its('headers.x-unfurl-authtype') { should eq "private" }
    end
  end
end
