terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 15.7"
    }
    local = {
      source = "hashicorp/local"
    }
  }
}

provider "gitlab" {
  base_url = var.gitlab_url
  token    = var.gitlab_token
}

resource "gitlab_user" "fixture_user" {
  for_each = var.users

  name           = title(replace(each.key, "-", " "))
  username       = each.key
  email          = "${each.key}@example.com"
  is_external    = lookup(each.value, "is_external", false)
  reset_password = true
}

resource "gitlab_personal_access_token" "fixture_user_token" {
  for_each = var.users

  user_id = gitlab_user.fixture_user[each.key].id
  name    = "Tyk middleware testing token"
  scopes  = ["read_api", "read_user", "read_repository"]

  depends_on = [
    gitlab_user.fixture_user
  ]
}

resource "gitlab_group" "fixture_group" {
  for_each = var.groups

  name             = title(replace(each.key, "-", " "))
  path             = each.key
  description      = "Tyk middleware testing group"
  visibility_level = each.value.visibility
}

resource "gitlab_project" "fixture_project" {
  for_each = var.projects

  path                   = split("/", each.key)[1]
  namespace_id           = gitlab_group.fixture_group[split("/", each.key)[0]].id
  name                   = title(replace(split("/", each.key)[1], "-", " "))
  description            = "Tyk middleware testing project"
  initialize_with_readme = true
  visibility_level       = each.value.visibility
  # turn off features we don't need for dummy projects
  issues_enabled         = false
  merge_requests_enabled = false
  wiki_enabled           = false
  packages_enabled       = false

  depends_on = [
    gitlab_user.fixture_user,
    gitlab_group.fixture_group
  ]
}

# transform { project: members: [users] } into flat mapping
locals {
  memberships = flatten(
    [
      for p, opts in var.projects :
      [
        for user, level in opts.members :
        { "project" = p, "user" = user, "level" = level }
      ]
    ]
  )
}

resource "gitlab_project_membership" "fixture_project_members" {
  for_each = {
    for mb in local.memberships :
    "${mb.project}:${mb.user}" => mb
  }

  project_id   = gitlab_project.fixture_project[each.value.project].id
  user_id      = gitlab_user.fixture_user[each.value.user].id
  access_level = each.value.level

  depends_on = [
    gitlab_user.fixture_user,
    gitlab_project.fixture_project
  ]
}

# create inspec inputs file from resources
resource "local_file" "inspec_inputs" {
  filename        = "../test-inputs.yml"
  file_permission = "600"
  content = yamlencode(
    {
      "gitlab_url" = var.gitlab_url
      "projects" = {
        for path, opts in var.projects :
        opts.visibility => path
      }
      "users" = merge(
        {
          for name, opts in var.users :
          reverse(split("-", name))[0] => gitlab_personal_access_token.fixture_user_token[name].token
        },
        # save admin token provided as admin user
        { "admin" = var.gitlab_token }
      )
    }
  )
}
