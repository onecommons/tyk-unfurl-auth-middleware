variable "gitlab_url" {
  description = "The Gitlab instance to operate on"
}

variable "gitlab_token" {
  description = "Admin user token to authenticate to $gitlab_url"
}

variable "users" {
  description = "List of users to create and any properties to set"

  default = {
    "tyk-test-external"  = { "is_external" = true }
    "tyk-test-normal"    = {}
    "tyk-test-member"    = {}
    "tyk-test-developer" = {}
  }
}

variable "groups" {
  description = "List of groups to create and any properties to set"

  default = {
    "tyk-tests" = { "visibility" = "public" }
  }
}

variable "projects" {
  description = "List of projects to create and any properties to set"

  default = {
    "tyk-tests/tyk-public" = {
      "visibility" = "public"
      "members" = {
        "tyk-test-member"    = "guest",
        "tyk-test-developer" = "developer",
      }
    },
    "tyk-tests/tyk-internal" = {
      "visibility" = "internal"
      "members" = {
        "tyk-test-member"    = "guest",
        "tyk-test-developer" = "developer",
      }
    },
    "tyk-tests/tyk-private" = {
      "visibility" = "private"
      "members" = {
        "tyk-test-member"    = "guest",
        "tyk-test-developer" = "developer",
      }
    }
  }
}
