# Integration testing

These tests use [Cinc Auditor](https://cinc.sh/start/auditor/), a community rebuild of [InSpec](https://docs.chef.io/inspec) from the Chef project.

## Running the tests

### 1. Set up test fixtures

Terraform to create the test fixtures is provided under `setup/`. This will create the `test-inputs.yml` file for Inspec to use. Provide the URL of the Gitlab instance to operate on, and an **admin** access token (creating users needs admin permissions).

```shell
cd tests/setup/
terraform plan \
  -var gitlab_url="https://your-gitlab.example.com" \
  -var gitlab_token="$ADMIN_TOKEN" \
  -out .tfplan

terraform apply .tfplan
```

> 📝 These fixtures tested against in the CI pipeline are pre-created on Staging.


### 2. Run the tests

- Via Docker Compose (full stack tests)

  ```shell
  cd tests/
  docker compose up -d --build --wait # inspec container will exit(100) on fail
  docker compose logs inspec          # inspec output is captured by docker
  ```

- Via Docker Compose (use existing test projects on Staging)

  ```shell
  cd tests/
  docker compose -f docker-compose.staging.yaml up -d --build --wait
  docker compose logs inspec          # inspec output is captured by docker
  ```

- Manually:

  > This assumes Tyk is already running and configured with the middleware.

  ```shell
  cd tests/
  bundle install --gemfile ./inspec/Gemfile
  bundle exec cinc-auditor exec ./inspec \
    --input-file ./test-inputs.yml \
    --input tyk_url=http://your-tyk.example.com
  ```
