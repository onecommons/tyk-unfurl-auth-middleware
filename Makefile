# default locations
OUTPUT_FILE ?= bundles/unfurl-auth.zip
TYK ?= /opt/tyk-gateway/tyk

all: build

prereqs:
ifeq (, $(shell which zip))
	$(error "zip not found")
endif

ifeq (, $(shell which pip3))
	$(error "pip3 not found")
endif

ifeq (, $(shell which ${TYK}))
	$(error "${TYK} not found, set $$TYK to binary location")
endif

build: prereqs
# install to vendored dir to be bundled
	python3 -m pip install -q -r requirements.txt --ignore-installed --target vendor/python/site-packages

# build bundle
	${TYK} bundle build -y -o "${OUTPUT_FILE}"

# add vendored files to manifest json
	zip -uqr "${OUTPUT_FILE}" vendor/

# for use with the dev container only!
# random version suffix is used to force a redownload
VER := $(shell bash -c 'echo $$RANDOM')
deploy:
	OUTPUT_FILE="bundles/unfurl-auth-${VER}.zip" make build

	cp -f container/test-apis/*.json /opt/tyk-gateway/apps/
	sed -i -E "s/unfurl-auth.+zip/unfurl-auth-${VER}.zip/" /opt/tyk-gateway/apps/*.json

	systemctl restart tyk-gateway.service

clean:
	rm -r vendor/ bundles/*.zip
