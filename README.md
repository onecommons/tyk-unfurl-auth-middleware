# Unfurl Cloud Dashboard auth middleware

This [Tyk middleware](https://tyk.io/docs/plugins/supported-languages/rich-plugins/python/custom-auth-python-tutorial/)
checks whether a user on a Gitlab instance (e.g. [Unfurl Cloud](https://unfurl.cloud)) has visibility to a specified
project.

This is used as part of the authorization and validation chain for Open Cloud resources on Unfurl Cloud.

### Build Instructions

Tyk middleware is bundled as a zip and needs to be hosted on an external webserver. Tyk Gateway pulls the zip at runtime and executes it in process. The `tyk` cli includes a `bundle build` command, but this project needs to also bundle the vendored `python-gitlab` dependency. This vendoring is done in the Makefile.

```sh
make build
```

- If `tyk` is not installed at the default `/opt/tyk-gateway` path, set `$TYK` to the binary path.
- To build the bundle to a specific path, set `$OUTPUT_FILE`.

```sh
TYK=/usr/local/bin/tyk OUTPUT_FILE=bundles/custom-name.zip make build
```

A development container setup is available under `container/`:

```sh
docker build --tag tyk-testing -f container/Dockerfile .
docker run -d --name tyk -v ./:/opt/tyk-unfurl-auth-middleware \
  -p 8080:8080 -e 'UNFURL_CLOUD_URL=https://unfurl.cloud' tyk-testing
docker exec -it tyk make deploy # to reload middleware and api definitions
```

### Configuration

The Tyk global config (`tyk.conf`) needs these fields set to use Python middleware:

```json
{
  "coprocess_options": {
    "enable_coprocess": true,
    "python_path_prefix": "/opt/tyk-gateway"
  },
  "enable_bundle_downloader": true,
  // "latest" package builds from the latest commit on main
  // Per-commit and per-branch builds are also provided
  "bundle_base_url": "https://gitlab.com/api/v4/projects/onecommons%2Ftyk-unfurl-auth-middleware/packages/generic/unfurl-auth/latest/",
  "enable_context_vars": true
}
```

An example API definition is included under `tests/`. The important bits:

```json
{
  "custom_middleware_bundle": "unfurl-auth.zip",
  "use_keyless": false,
  "enable_coprocess_auth": true,
  "config_data": {
    // see Permission Checks below
  }
}
```

### Authentication

This middleware behaves like Gitlab's API, and will accept either:

- Gitlab user access token as `private_token` query param / `PRIVATE_TOKEN` header
- Gitlab session cookie

If no authentication method is provided, anonymous guest access is used.

### Permission Checks

The following permission checks are supported:

##### Gitlab user state

- `auth_access`: The user's minimum account state (`external`, `normal`, or `admin`)

##### Gitlab project access

- `auth_project`: Project path or ID
- `auth_project_access`: the minimum permission level (see [Gitlab docs](https://docs.gitlab.com/ee/api/members.html#valid-access-levels) for names, plus `public` for public visibility). Defaults to `owner` if not set.

##### Gitlab group access

- `auth_group`: Group path or id
- `auth_group_access` the minimum permission level (see [Gitlab docs](https://docs.gitlab.com/ee/api/members.html#valid-access-levels) for names). Defaults to `owner` if not set.

> :pencil: **Note:** All checks provided need to be met for the check to succeed. If none are provided, *access is allowed by default*.

> :pencil: **Note:** Options can be specified either via `config_data` in the API definition or via query parameters in each request, and will be merged together. Fields from `config_data` have higher precedence and cannot be overridden by query parameters.

#### Separate permissions for read and write access

`auth_access`, `auth_project_access`, and `auth_group_access` can each optionally be assigned a dictionary with `read` and `write` keys set to different access levels. For example:

```json
"config_data": {
  "auth_project": "someuser/example",
  "auth_project_access": {
    "read": "guest",
    "write": "maintainer"
  }
}
```

A HTTP `GET` or `HEAD` request will be authenticated using the `read` permission while any other HTTP method (e.g. `POST`, `PUT`, or `DELETE`) will be authenticated using the `write` permission.

#### Optional fields

These are only sourced from the API definition `config_data`.

##### Caching

- `ttl`: Time in seconds to cache the authorization result for the user. Defaults to `500` (5 min). Bumping this will speed up requests, as Tyk will not call out to Gitlab after the initial call.

  This caching is done for identical requests to an api, not for any requests. Different users or any changed parameters are cached separately.

##### Rate limits

- `user_rate`: Number of requests permitted...
- `user_per`: ...in this amount of time (seconds)

  Defaults to 100 requests per 10 seconds. This default limit is intentionally left low.

  This limit is done for identical requests to an api, not for any request. Different users or any changed parameters are limited separately.

  These fields also support separate `read` and `write` values that behave like the `auth_` fields above.

### Examples

<!-- HTML table is used here since Markdown tables don't allow code blocks -->
<table>
<tr>
<td>

  ```json
  "config_data": {
    "auth_access": "admin",
    "ttl": 3600,
    "user_rate": 1000,
    "user_per": 10
  }
  ```

</td>
<td>

  User must be a Gitlab admin, caches auth result for 1 hour, and allows a higher rate limit of 1000 requests per 10 seconds. *(no project or group membership checks are performed)*

</td>
</tr>
<tr>
<td>

  ```json
  "config_data": {
    "auth_access": "normal",
    "auth_project": "someuser/example",
    "auth_project_access": "maintainer"
  }
  ```

</td>
<td>

  User must be a normal user (i.e. not external), and have Maintainer or higher access on `someuser/example`.

</td>
</tr>
<tr>
<td>

  ```json
  "config_data": {
    "auth_project_access": "guest"
  }
  ```

</td>
<td>

  If `auth_project` query parameter is specified, the user must have Guest or higher access on that project.

</td>
</tr>
<tr>
<td>

  ```json
  "config_data": {
    "auth_access": {
      "read": "normal",
      "write": "admin"
    },
    "user_rate": {
      "read": 1000,
      "write": 10
    },
    "user_per": {
      "read": 10,
      "write": 86400
    }
  }
  ```

</td>
<td>

  Read access is allowed for normal or higher users, and limited to 1000 requests per 10 seconds.

  Write access is restricted to admins and limited to 10 requests per day.

</td>
</tr>
</table>
