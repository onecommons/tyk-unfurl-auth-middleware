import sys
from unittest.mock import Mock, patch

import itertools
import pytest

from .fixtures import gl, external_user, normal_user, admin_user

with patch.dict(sys.modules, {"tyk.gateway": Mock()}):
    from check import Check


# default all checks to false
@pytest.fixture()
def check(monkeypatch):
    monkeypatch.setattr(Check, "no_checks", lambda _: None)
    monkeypatch.setattr(Check, "check_public", lambda _i, _j=None: None)
    monkeypatch.setattr(Check, "check_user", lambda _: None)
    monkeypatch.setattr(Check, "check_project", lambda _: None)
    monkeypatch.setattr(Check, "check_group", lambda _: None)

    return Check


def all_combinations(*args):
    "itertools.combinations() across all lengths"
    return itertools.chain.from_iterable(
        itertools.combinations(args, i + 1) for i in range(len(args))
    )


class TestAllowed:
    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, False),
            (external_user, "external-user"),
            (normal_user, "normal-user"),
            (admin_user, "admin-user"),
        ],
        indirect=["gl"],
    )
    def test_all_none(self, gl, check, expected):
        "Verify auth is skipped when all auth methods are none"

        check = Check(gl, {}, not gl.user)
        assert check.allowed()[0] == expected

    @pytest.mark.parametrize(
        "gl",
        [None, external_user, normal_user, admin_user],
        indirect=["gl"],
    )
    def test_all_false(self, monkeypatch, gl, check):
        "Verify auth fails when all auth methods are false"

        with monkeypatch.context() as mp:
            [
                mp.setattr(Check, m, lambda _i, _j=None: False)
                for m in (
                    "no_checks",
                    "check_public",
                    "check_user",
                    "check_project",
                    "check_group",
                )
            ]

            check = Check(gl, {}, not gl.user)
            assert check.allowed()[0] is False

    @pytest.mark.parametrize(
        "methods",
        all_combinations("check_user", "check_project", "check_group"),
    )
    @pytest.mark.parametrize(
        "gl",
        [None, external_user, normal_user, admin_user],
        indirect=["gl"],
    )
    def test_any_false(self, monkeypatch, gl, check, methods):
        "Verify auth fails when any or all methods are false"

        with monkeypatch.context() as mp:
            [mp.setattr(Check, m, lambda _: False) for m in methods]

            check = Check(gl, {}, not gl.user)
            assert check.allowed()[0] is False

    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, "[guest]"),
            (external_user, "external-user"),
            (normal_user, "normal-user"),
            (admin_user, "admin-user"),
        ],
        indirect=["gl"],
    )
    def test_no_checks(self, monkeypatch, gl, check, expected):
        "Verify correct username is returned for no_params result"

        with monkeypatch.context() as mp:
            mp.setattr(Check, "no_checks", lambda _: True)

            check = Check(gl, {}, not gl.user)
            assert check.allowed()[0] == expected

    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, "[guest]"),
            (external_user, "external-user"),
            (normal_user, "normal-user"),
            (admin_user, "admin-user"),
        ],
        indirect=["gl"],
    )
    def test_check_public(self, monkeypatch, gl, check, expected):
        "Verify correct username is returned for public_check result"

        with monkeypatch.context() as mp:
            mp.setattr(Check, "check_public", lambda _i, _j=None: True)

            check = Check(gl, {}, not gl.user)
            assert check.allowed()[0] == expected

    @pytest.mark.parametrize(
        "methods",
        all_combinations("check_user", "check_project", "check_group"),
    )
    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, False),
            (external_user, "external-user"),
            (normal_user, "normal-user"),
            (admin_user, "admin-user"),
        ],
        indirect=["gl"],
    )
    def test_checks_with_auth(self, monkeypatch, gl, check, methods, expected):
        "Verify correct username is returned for auth checks result"

        with monkeypatch.context() as mp:
            [mp.setattr(Check, m, lambda _: True) for m in methods]

            check = Check(gl, {}, not gl.user)
            assert check.allowed()[0] == expected
