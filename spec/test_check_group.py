import sys
from unittest.mock import Mock, patch

import gitlab
import pytest

from .fixtures import (
    developer_group,
    gl,
    gl_group,
    other_group,
    guest_group,
    maintainer_group,
    minimal_group,
    owner_group,
    reporter_group,
)

with patch.dict(sys.modules, {"tyk.gateway": Mock()}):
    from check import Check


class TestGroupRequirements:
    """
    Test `Check.check_group` behaves as expected
    """

    @pytest.mark.parametrize(
        "access_level",
        [
            None,
            "public",
            "minimal",
            "guest",
            "reporter",
            "developer",
            "maintainer",
            "owner",
        ],
    )
    def test_private_group(self, gl_group, access_level):
        "when auth_group is private"

        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": access_level,
        }

        check = Check(gl_group, params, False)

        # caught by accepted() wrapper
        with pytest.raises(gitlab.exceptions.GitlabGetError):
            check.check_group()
            gl_group.groups.get.assert_called_with("mocks/test-group")


    @pytest.mark.parametrize(
        ["gl_group", "expected"],
        [
            (other_group, False),
            (minimal_group, False),
            (guest_group, False),
            (reporter_group, False),
            (developer_group, False),
            (maintainer_group, False),
            (owner_group, True),
        ],
        indirect=["gl_group"],
    )
    def test_none_requirement(self, gl_group, expected):
        "behaves like owner requirement when group access is none"

        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": None,
        }

        check = Check(gl_group, params, False)

        assert check.check_group() == expected
        gl_group.groups.get.assert_called_with("mocks/test-group")


    @pytest.mark.parametrize(
        ["gl_group", "expected"],
        [
            (other_group, False),
            (minimal_group, True),
            (guest_group, True),
            (reporter_group, True),
            (developer_group, True),
            (maintainer_group, True),
            (owner_group, True),
        ],
        indirect=["gl_group"],
    )
    def test_minimal_requirement(self, gl_group, expected):
        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": "minimal",
        }

        check = Check(gl_group, params, False)

        assert check.check_group() == expected
        gl_group.groups.get.assert_called_with("mocks/test-group")

    @pytest.mark.parametrize(
        ["gl_group", "expected"],
        [
            (other_group, False),
            (minimal_group, False),
            (guest_group, True),
            (reporter_group, True),
            (developer_group, True),
            (maintainer_group, True),
            (owner_group, True),
        ],
        indirect=["gl_group"],
    )
    def test_guest_requirement(self, gl_group, expected):
        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": "guest",
        }

        check = Check(gl_group, params, False)

        assert check.check_group() == expected
        gl_group.groups.get.assert_called_with("mocks/test-group")

    @pytest.mark.parametrize(
        ["gl_group", "expected"],
        [
            (other_group, False),
            (minimal_group, False),
            (guest_group, False),
            (reporter_group, True),
            (developer_group, True),
            (maintainer_group, True),
            (owner_group, True),
        ],
        indirect=["gl_group"],
    )
    def test_reporter_requirement(self, gl_group, expected):
        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": "reporter",
        }

        check = Check(gl_group, params, False)

        assert check.check_group() == expected
        gl_group.groups.get.assert_called_with("mocks/test-group")

    @pytest.mark.parametrize(
        ["gl_group", "expected"],
        [
            (other_group, False),
            (minimal_group, False),
            (guest_group, False),
            (reporter_group, False),
            (developer_group, True),
            (maintainer_group, True),
            (owner_group, True),
        ],
        indirect=["gl_group"],
    )
    def test_developer_requirement(self, gl_group, expected):
        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": "developer",
        }

        check = Check(gl_group, params, False)

        assert check.check_group() == expected
        gl_group.groups.get.assert_called_with("mocks/test-group")

    @pytest.mark.parametrize(
        ["gl_group", "expected"],
        [
            (other_group, False),
            (minimal_group, False),
            (guest_group, False),
            (reporter_group, False),
            (developer_group, False),
            (maintainer_group, True),
            (owner_group, True),
        ],
        indirect=["gl_group"],
    )
    def test_maintainer_requirement(self, gl_group, expected):
        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": "maintainer",
        }

        check = Check(gl_group, params, False)

        assert check.check_group() == expected
        gl_group.groups.get.assert_called_with("mocks/test-group")

    @pytest.mark.parametrize(
        ["gl_group", "expected"],
        [
            (other_group, False),
            (minimal_group, False),
            (guest_group, False),
            (reporter_group, False),
            (developer_group, False),
            (maintainer_group, False),
            (owner_group, True),
        ],
        indirect=["gl_group"],
    )
    def test_owner_requirement(self, gl_group, expected):
        params = {
            "auth_group": "mocks/test-group",
            "auth_group_access": "owner",
        }

        check = Check(gl_group, params, False)

        assert check.check_group() == expected
        gl_group.groups.get.assert_called_with("mocks/test-group")
