import sys

import pytest
from unittest.mock import Mock, patch

from .fixtures import gl

with patch.dict(sys.modules, {"tyk.gateway": Mock()}):
    from check import Check


class TestHelperMethods:
    @pytest.mark.parametrize(
        "method,expected",
        [
            (None, "FOO_PERM"),
            ("GET", "FOO_PERM"),
            ("HEAD", "FOO_PERM"),
            ("DELETE", "FOO_PERM"),
            ("POST", "FOO_PERM"),
            ("PUT", "FOO_PERM"),
            ("PATCH", "FOO_PERM"),
        ],
    )
    def test_get_level(self, gl, method, expected):
        "get_level works for single values"

        fields = {
            "auth_access": "FOO_PERM",
            "method": method,
        }
        check = Check(gl, fields, True)
        assert check.get_level("auth_access") == expected

    @pytest.mark.parametrize(
        "method,expected",
        [
            (None, "READ_PERM"),
            ("GET", "READ_PERM"),
            ("HEAD", "READ_PERM"),
            ("DELETE", "WRITE_PERM"),
            ("POST", "WRITE_PERM"),
            ("PUT", "WRITE_PERM"),
            ("PATCH", "WRITE_PERM"),
        ],
    )
    def test_get_level_rw(self, gl, method, expected):
        "get_level works for separate read/write values"

        fields = {
            "auth_access": {"read": "READ_PERM", "write": "WRITE_PERM"},
            "method": method,
        }
        check = Check(gl, fields, True)
        assert check.get_level("auth_access") == expected

    def test_no_params(self, gl):
        "when no fields are specified"

        check = Check(gl, {}, False)
        assert check.no_checks()

    def test_other_params(self, gl):
        "when non-auth fields are specified"

        check = Check(gl, {"foo": "bar"}, False)
        assert check.no_checks()

    @pytest.mark.parametrize(
        "field",
        [
            {"auth_access": None},
            {"auth_project": None},
            {"auth_project_access": None},
            {"auth_group": None},
            {"auth_group_access": None},
        ],
    )
    def test_none_params(self, gl, field):
        "when auth field is present but null"

        check = Check(gl, field, False)
        assert check.no_checks()

    @pytest.mark.parametrize(
        "field",
        [
            {"auth_access": "normal"},
            {"auth_project": "foo/bar"},
            {"auth_project_access": "developer"},
            {"auth_group": "foo/bar"},
            {"auth_group_access": "developer"},
        ],
    )
    def test_with_params(self, gl, field):
        "when auth field is present with value"
        check = Check(gl, field, False)
        assert not check.no_checks()
