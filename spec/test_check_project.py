import sys
from unittest.mock import Mock, patch

import gitlab
import pytest

from .fixtures import (
    developer_project,
    gl,
    gl_project,
    other_project,
    guest_project,
    maintainer_project,
    minimal_project,
    owner_project,
    reporter_project,
)

with patch.dict(sys.modules, {"tyk.gateway": Mock()}):
    from check import Check


class TestProjectRequirements:
    """
    Test `Check.check_project` behaves as expected
    """

    @pytest.mark.parametrize(
        "access_level",
        [
            None,
            "public",
            "minimal",
            "guest",
            "reporter",
            "developer",
            "maintainer",
            "owner",
        ],
    )
    def test_private_project(self, gl_project, access_level):
        "when auth_project is private"

        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": access_level,
        }

        check = Check(gl_project, params, False)

        # caught by accepted() wrapper
        with pytest.raises(gitlab.exceptions.GitlabGetError):
            check.check_project()
            gl_project.projects.get.assert_called_with("mocks/test-project")


    @pytest.mark.parametrize(
        ["gl_project", "expected"],
        [
            (other_project, False),
            (minimal_project, False),
            (guest_project, False),
            (reporter_project, False),
            (developer_project, False),
            (maintainer_project, False),
            (owner_project, True),
        ],
        indirect=["gl_project"],
    )
    def test_none_requirement(self, gl_project, expected):
        "behaves like owner requirement when project access is none"

        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": None,
        }

        check = Check(gl_project, params, False)

        assert check.check_project() == expected
        gl_project.projects.get.assert_called_with("mocks/test-project")


    @pytest.mark.parametrize(
        ["gl_project", "expected"],
        [
            (other_project, False),
            (minimal_project, True),
            (guest_project, True),
            (reporter_project, True),
            (developer_project, True),
            (maintainer_project, True),
            (owner_project, True),
        ],
        indirect=["gl_project"],
    )
    def test_minimal_requirement(self, gl_project, expected):
        "when user is member of auth_project"

        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": "minimal",
        }

        check = Check(gl_project, params, False)

        assert check.check_project() == expected
        gl_project.projects.get.assert_called_with("mocks/test-project")

    @pytest.mark.parametrize(
        ["gl_project", "expected"],
        [
            (other_project, False),
            (minimal_project, False),
            (guest_project, True),
            (reporter_project, True),
            (developer_project, True),
            (maintainer_project, True),
            (owner_project, True),
        ],
        indirect=["gl_project"],
    )
    def test_guest_requirement(self, gl_project, expected):
        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": "guest",
        }

        check = Check(gl_project, params, False)

        assert check.check_project() == expected
        gl_project.projects.get.assert_called_with("mocks/test-project")

    @pytest.mark.parametrize(
        ["gl_project", "expected"],
        [
            (other_project, False),
            (minimal_project, False),
            (guest_project, False),
            (reporter_project, True),
            (developer_project, True),
            (maintainer_project, True),
            (owner_project, True),
        ],
        indirect=["gl_project"],
    )
    def test_reporter_requirement(self, gl_project, expected):
        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": "reporter",
        }

        check = Check(gl_project, params, False)

        assert check.check_project() == expected
        gl_project.projects.get.assert_called_with("mocks/test-project")

    @pytest.mark.parametrize(
        ["gl_project", "expected"],
        [
            (other_project, False),
            (minimal_project, False),
            (guest_project, False),
            (reporter_project, False),
            (developer_project, True),
            (maintainer_project, True),
            (owner_project, True),
        ],
        indirect=["gl_project"],
    )
    def test_developer_requirement(self, gl_project, expected):
        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": "developer",
        }

        check = Check(gl_project, params, False)

        assert check.check_project() == expected
        gl_project.projects.get.assert_called_with("mocks/test-project")

    @pytest.mark.parametrize(
        ["gl_project", "expected"],
        [
            (other_project, False),
            (minimal_project, False),
            (guest_project, False),
            (reporter_project, False),
            (developer_project, False),
            (maintainer_project, True),
            (owner_project, True),
        ],
        indirect=["gl_project"],
    )
    def test_maintainer_requirement(self, gl_project, expected):
        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": "maintainer",
        }

        check = Check(gl_project, params, False)

        assert check.check_project() == expected
        gl_project.projects.get.assert_called_with("mocks/test-project")

    @pytest.mark.parametrize(
        ["gl_project", "expected"],
        [
            (other_project, False),
            (minimal_project, False),
            (guest_project, False),
            (reporter_project, False),
            (developer_project, False),
            (maintainer_project, False),
            (owner_project, True),
        ],
        indirect=["gl_project"],
    )
    def test_owner_requirement(self, gl_project, expected):
        params = {
            "auth_project": "mocks/test-project",
            "auth_project_access": "owner",
        }

        check = Check(gl_project, params, False)

        assert check.check_project() == expected
        gl_project.projects.get.assert_called_with("mocks/test-project")
