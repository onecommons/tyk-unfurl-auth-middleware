import pytest
from unittest.mock import Mock, MagicMock

import gitlab


@pytest.fixture()
def gl(request):
    # use indirect param for user if specified
    user = getattr(request, "param", None)

    return Mock(
        name="gitlab mock", user=user, projects={}, groups={}, url="https://gitlab.test"
    )


# gl.user
external_user = Mock(
    name="External User",
    username="external-user",
    id=1,
    state="active",
    external=True,
    attributes={},
)

normal_user = Mock(
    name="Normal User",
    username="normal-user",
    id=2,
    state="active",
    external=False,
    attributes={},
)

admin_user = Mock(
    name="Admin User",
    username="admin-user",
    id=3,
    state="active",
    external=False,
    attributes={"is_admin": True},
)


@pytest.fixture
def gl_project(request):
    "gl fixture with project membership user"

    # use indirect param for project if specified
    project = getattr(request, "param", None)

    if project:
        return Mock(
            name="gitlab mock",
            user=normal_user,
            projects=MagicMock(
                get=Mock(return_value=project),
            ),
            # projects={
            #     "mocks/test-project": project
            # }
        )
    else:
        # if user cannot access project, api raises exception
        return Mock(
            name="gitlab mock",
            user=normal_user,
            projects=MagicMock(
                name="Private project error",
                get=Mock(side_effect=gitlab.exceptions.GitlabGetError),
            ),
        )


minimal_member = Mock(name="Minimal Member", access_level=5)
guest_member = Mock(name="Guest Member", access_level=10)
reporter_member = Mock(name="Reporter Member", access_level=20)
developer_member = Mock(name="Developer Member", access_level=30)
maintainer_member = Mock(name="Maintainer Member", access_level=40)
owner_member = Mock(name="Owner Member", access_level=50)


# gl.project
other_project = Mock(
    name="Non-member Project",
    path="test-project",
    path_with_namespace="mocks/test-project",
    members_all={},
)

minimal_project = Mock(
    name="Minimal Project",
    path="test-project",
    path_with_namespace="mocks/test-project",
    members_all=Mock(
        get=Mock(return_value=minimal_member),
    ),
)

guest_project = Mock(
    name="Guest Project",
    path="test-project",
    path_with_namespace="mocks/test-project",
    members_all=Mock(
        get=Mock(return_value=guest_member),
    ),
)

reporter_project = Mock(
    name="Reporter Project",
    path="test-project",
    path_with_namespace="mocks/test-project",
    members_all=Mock(
        get=Mock(return_value=reporter_member),
    ),
)

developer_project = Mock(
    name="Developer Project",
    path="test-project",
    path_with_namespace="mocks/test-project",
    members_all=Mock(
        get=Mock(return_value=developer_member),
    ),
)

maintainer_project = Mock(
    name="Maintainer Project",
    path="test-project",
    path_with_namespace="mocks/test-project",
    members_all=Mock(
        get=Mock(return_value=maintainer_member),
    ),
)

owner_project = Mock(
    name="Owner Project",
    path="test-project",
    path_with_namespace="mocks/test-project",
    members_all=Mock(
        get=Mock(return_value=owner_member),
    ),
)


# gl.group
@pytest.fixture
def gl_group(request):
    "gl fixture with group membership user"

    # use indirect param for group if specified
    group = getattr(request, "param", None)

    if group:
        return Mock(
            name="gitlab mock",
            user=normal_user,
            groups=MagicMock(
                get=Mock(return_value=group),
            ),
            # groups={
            #     "mocks/test-group": group
            # }
        )
    else:
        # if user cannot access group, api raises exception
        return Mock(
            name="gitlab mock",
            user=normal_user,
            groups=MagicMock(
                name="Private group error",
                get=Mock(side_effect=gitlab.exceptions.GitlabGetError),
            ),
        )


other_group = Mock(
    name="Non-member Project",
    path="test-group",
    path_with_namespace="mocks/test-group",
    members_all={},
)

minimal_group = Mock(
    name="Minimal Project",
    path="test-group",
    path_with_namespace="mocks/test-group",
    members_all=Mock(
        get=Mock(return_value=minimal_member),
    ),
)

guest_group = Mock(
    name="Guest Project",
    path="test-group",
    path_with_namespace="mocks/test-group",
    members_all=Mock(
        get=Mock(return_value=guest_member),
    ),
)

reporter_group = Mock(
    name="Reporter Project",
    path="test-group",
    path_with_namespace="mocks/test-group",
    members_all=Mock(
        get=Mock(return_value=reporter_member),
    ),
)

developer_group = Mock(
    name="Developer Project",
    path="test-group",
    path_with_namespace="mocks/test-group",
    members_all=Mock(
        get=Mock(return_value=developer_member),
    ),
)

maintainer_group = Mock(
    name="Maintainer Project",
    path="test-group",
    path_with_namespace="mocks/test-group",
    members_all=Mock(
        get=Mock(return_value=maintainer_member),
    ),
)

owner_group = Mock(
    name="Owner Project",
    path="test-group",
    path_with_namespace="mocks/test-group",
    members_all=Mock(
        get=Mock(return_value=owner_member),
    ),
)
