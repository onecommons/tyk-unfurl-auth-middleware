import sys

import pytest
from unittest.mock import Mock, patch

from .fixtures import gl, external_user, normal_user, admin_user

with patch.dict(sys.modules, {"tyk.gateway": Mock()}):
    from check import Check


class TestCheckUser:
    "Test `check_user` behaves as expected"

    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, None),
            (external_user, None),
            (normal_user, None),
            (admin_user, None),
        ],
        indirect=["gl"],
    )
    def test_no_requirement(self, gl, expected):
        "when auth_access is None"

        params = {"auth_access": None}

        check = Check(gl, params, False)
        assert check.check_user() == expected

    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, False),
            (external_user, True),
            (normal_user, True),
            (admin_user, True),
        ],
        indirect=["gl"],
    )
    def test_external_requirement(self, gl, expected):
        "when auth_access is external"

        params = {"auth_access": "external"}

        check = Check(gl, params, False)
        assert check.check_user() == expected

    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, False),
            (external_user, False),
            (normal_user, True),
            (admin_user, True),
        ],
        indirect=["gl"],
    )
    def test_normal_requirement(self, gl, expected):
        "when auth_access is normal"

        params = {"auth_access": "normal"}

        check = Check(gl, params, False)
        assert check.check_user() == expected

    @pytest.mark.parametrize(
        "gl,expected",
        [
            (None, False),
            (external_user, False),
            (normal_user, False),
            (admin_user, True),
        ],
        indirect=["gl"],
    )
    def test_admin_requirement(self, gl, expected):
        "when auth_access is admin"

        params = {"auth_access": "admin"}

        check = Check(gl, params, False)
        assert check.check_user() == expected
