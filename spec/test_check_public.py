import sys
from unittest.mock import Mock, patch

import gitlab
import pytest

from .fixtures import admin_user, external_user, gl, normal_user

with patch.dict(sys.modules, {"tyk.gateway": Mock()}):
    from check import Check


class TestPublicRequirement:
    """
    Test `Check.check_public` behaves as expected when auth_access is public
    """

    @pytest.mark.parametrize(
        "gl",
        [None, external_user, normal_user, admin_user],
        indirect=True,
    )
    def test_public_project(self, gl):
        "when accessed project is public"

        params = {"auth_project": "example/test", "auth_project_access": "public"}

        gl.http_get = Mock(return_value="something that is not an error")

        check = Check(gl, params, False, gl)
        assert check.check_public() is True

    @pytest.mark.parametrize(
        "gl",
        [None, external_user, normal_user, admin_user],
        indirect=True,
    )
    def test_internal_or_private_project(self, gl):
        "when accessed project is not public"

        params = {"auth_project": "example/test", "auth_project_access": "public"}

        gl.http_get = Mock(side_effect=gitlab.exceptions.GitlabHttpError)

        check = Check(gl, params, False, gl)
        assert check.check_public() is False

    @pytest.mark.parametrize(
        "gl",
        [None, external_user, normal_user, admin_user],
        indirect=True,
    )
    def test_non_public_api(self, gl):
        "when api record is not public"

        params = {"auth_project": "example/test", "auth_project_access": "developer"}

        check = Check(gl, params, False, gl)
        assert check.check_public() is False
