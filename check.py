import functools
import urllib.parse
from typing import Union

import gitlab
from tyk.gateway import TykGateway as tyk


AUTH_FIELDS = [
    "auth_access",
    "auth_project",
    "auth_project_access",
    "auth_group",
    "auth_group_access",
]


class Check:
    def __init__(self, gl_session, search_params, guest=True, anon_gl_session=None) -> None:
        self.gl = gl_session
        self.params = search_params
        self.guest = guest
        # if somehow method was not set, assume GET
        self.method = search_params.get("method") or "GET"
        # create second client without auth (optional param for spec)
        self.anon_gl = anon_gl_session or gitlab.Gitlab(self.gl.url)

    def allowed(self) -> (bool, str):
        params_without_none = {k: v for k, v in self.params.items() if v}
        tyk.log(f"UF: auth request with {params_without_none}...", "debug")
        try:
            # these dont need to auth() with gitlab
            # use anon_gl here so internal projects are not set
            if self.no_checks() or self.check_public():
                return ("[guest]" if self.guest else self.gl.user.username, "public")

            # if we have auth do auth checks
            if not self.guest:
                # self.gl.auth() # redundant, already auth'd

                # check for public/internal projects here
                # this will use auth'd gl client to check, which will catch internal projects
                if self.check_internal():
                    return (self.gl.user.username, "private")

                results = [
                    # None means check was skipped, ignore
                    # explicit False on failure
                    result is None or result
                    for result in [
                        self.check_user(),
                        self.check_project(),
                        self.check_group(),
                    ]
                ]

                # return username if checks pass
                if all(results):
                    return (self.gl.user.username, "private")

            # if nothing passed, return false
            return (False, None)

        except gitlab.exceptions.GitlabError as e:
            tyk.log(f"UF:   caught error: {type(e)} {e}", "debug")
            # fetching project or etc failed, reject attempt
            return (False, None)

    def get_level(self, key, default=None) -> Union[str, None]:
        "Get access level for `key` based on if request is read or write"

        access = self.params.get(key, default)

        if not isinstance(access, dict):
            return access  # str or None
        if self.method in ["GET", "HEAD"]:
            return access.get("read", default)
        else:
            return access.get("write", default)

    def if_needed(auth_func) -> Union[callable, None]:
        "Skips the decorated function if the associated keys are not present in the auth parameters"

        @functools.wraps(auth_func)
        def inner(self):
            type = auth_func.__name__[6:]

            # only call decorated function if parameters are given
            if (
                (type == "user" and self.get_level("auth_access"))
                or self.get_level(f"auth_{type}")
                or self.get_level(f"auth_{type}_access")
            ):
                return auth_func(self)
            else:
                tyk.log(f"UF:   {type} params not set, skipping...", "debug")
                return None  # skipped

        return inner

    def log_result(auth_func) -> callable:
        "Prints the result of the decorated function to the tyk log"

        @functools.wraps(auth_func)
        def inner(self):
            tyk.log(f"UF:   checking {auth_func.__name__}...", "debug")
            result = auth_func(self)
            tyk.log(f"UF:     result: {result}", "debug")
            return result

        return inner

    @log_result
    def no_checks(self) -> bool:
        "Allow guest access if no auth checks are set (all are none)"
        return not any([self.get_level(f) for f in AUTH_FIELDS])

    @log_result
    def check_public(self) -> bool:
        return self._check_access(self.anon_gl)

    @log_result
    def check_internal(self) -> bool:
        return self._check_access(self.gl)

    def _check_access(self, gl) -> bool:
        # used by check_public and check_internal, but with/without auth

        # if project is not public, fetching project via gl-python doesn't
        # fail until properties are accessed. however, manually making the
        # api call does fail immediately
        auth_project = self.get_level("auth_project")

        # this check only works if there is a project to check
        # return true to allow other checks to happen (e.g. account level)
        if auth_project is None:
            tyk.log("UF:     no project specified, failed", "debug")
            return False

        # if requested permissions is not public, return false to fall
        # through to auth membership checks for higher levels
        if self.get_level("auth_project_access") != "public":
            tyk.log("UF:     project access is not public, failed", "debug")
            return False

        # encode slashes if path id given
        auth_project = urllib.parse.quote_plus(auth_project)

        try:
            # 401 caught by `except` below
            gl.http_get(f"/projects/{auth_project}")

            return True  # if no exception, its publically accessible
        except gitlab.exceptions.GitlabError as e:
            # fetching project failed, unauthorized
            tyk.log(f"UF:   error accessing public project: {type(e)} {e}", "debug")
            return False

    @if_needed
    @log_result
    def check_user(self) -> bool:
        # hierarchy:
        # external 10 < normal 20 < admin 30
        user_perm = 0
        if getattr(self.gl, "user", False):
            user_perm = 20 if self.gl.user.state == "active" else user_perm
            # external check overrides normal user
            user_perm = 10 if self.gl.user.external else user_perm
            # use .attributes.get to avoid KeyError if not present
            user_perm = 30 if self.gl.user.attributes.get("is_admin") else user_perm

        expected_perm = {
            None: 0,
            "external": 10,
            "normal": 20,
            "admin": 30,
        }[self.get_level("auth_access")]

        return user_perm >= expected_perm

    @if_needed
    @log_result
    def check_project(self) -> bool:
        # default to 'owner' check if access not specified
        project_name = self.get_level("auth_project")
        project_access = self.get_level("auth_project_access") or "owner"

        # can't check unless we have project name!
        if project_name is None:
            return False

        access_level = {
            "public": gitlab.const.MINIMAL_ACCESS,
            "minimal": gitlab.const.MINIMAL_ACCESS,
            "guest": gitlab.const.GUEST_ACCESS,
            "reporter": gitlab.const.REPORTER_ACCESS,
            "developer": gitlab.const.DEVELOPER_ACCESS,
            "maintainer": gitlab.const.MAINTAINER_ACCESS,
            "owner": gitlab.const.OWNER_ACCESS,
        }[project_access]

        # fetch project by name
        project = self.gl.projects.get(project_name)

        # get self on project
        project_user = project.members_all.get(self.gl.user.id)

        # allow auth if user can read the project
        return project_user is not None and project_user.access_level >= access_level

    @if_needed
    @log_result
    def check_group(self) -> bool:
        # default to 'owner' check if access not specified
        group_name = self.get_level("auth_group")
        group_access = self.get_level("auth_group_access") or "owner"

        # can't check unless we have group name!
        if group_name is None:
            return False

        access_level = {
            "public": gitlab.const.MINIMAL_ACCESS,
            "minimal": gitlab.const.MINIMAL_ACCESS,
            "guest": gitlab.const.GUEST_ACCESS,
            "reporter": gitlab.const.REPORTER_ACCESS,
            "developer": gitlab.const.DEVELOPER_ACCESS,
            "maintainer": gitlab.const.MAINTAINER_ACCESS,
            "owner": gitlab.const.OWNER_ACCESS,
        }[group_access]

        # fetch group by name
        group = self.gl.groups.get(group_name)

        # get self on group
        group_user = group.members_all.get(self.gl.user.id)

        # allow auth if user can read the group
        return group_user is not None and group_user.access_level >= access_level
